package com.yuli.devfestcompose
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.yuli.devfestcompose.ui.theme.DevfestComposeTheme

class MainActivity : ComponentActivity() {
    val userName = mutableStateOf("")
    val userPassword = mutableStateOf("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DevfestComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ImageLogin(userName,userPassword)
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

fun showToast(context: Context,text:String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}
@Composable
fun ImageLogin(userName: MutableState<String>, userPassword:MutableState<String>){
    Box{
        Column {
            Box{
                val imageForeground: Painter = painterResource(id = R.drawable.ic_launcher_foreground)
                val imageBackground: Painter = painterResource(id = R.drawable.ic_launcher_background)
                Image(painter = imageBackground,contentDescription = "")
                Image(painter = imageForeground,contentDescription = "")
            }
            UserLogin(userName,userPassword)
        }

    }
}
@Composable
fun UserLogin(userName: MutableState<String>, userPassword:MutableState<String>){
    val context = LocalContext.current
    Box{
        Column(modifier = Modifier.fillMaxSize()) {
            val warningText =  remember { mutableStateOf("") }
            Text(text = stringResource(id = R.string.user_name))
            TextField(value = userName.value, onValueChange = {
                userName.value = it.replace("_","")
                warningText.value = checkWarningText(userName.value,userPassword.value)
            })
            Text(text = stringResource(id = R.string.user_password))
            TextField(value = userPassword.value, onValueChange = {
                userPassword.value = it
                warningText.value = checkWarningText(userName.value,userPassword.value)
            })
            if(warningText.value.isNotEmpty()){
                Text(text = warningText.value)
            }
            Row{
                OutlinedButton(onClick = {showToast(context,"${userName.value}, ${userPassword.value}")}) {
                    Text(stringResource(id = R.string.login))
                }
                OutlinedButton(onClick = {
                    userName.value = ""
                    userPassword.value = ""
                }) {
                    Text(stringResource(id = R.string.clear))
                }
            }
        }
    }
}

fun checkWarningText(userName: String, userPassword: String): String {
    val strBuilder = StringBuilder()
    if(userName.length>6){
        strBuilder.appendLine("User Name to long!")
    }
    if(userPassword.length>6){
        strBuilder.appendLine("User Password to long!")
    }
    return strBuilder.toString()

}

@Preview(showBackground = true)
@Composable
fun LoginPreview() {
    val userName = remember { mutableStateOf("john") }
    val userPassword = remember { mutableStateOf("apple") }


    DevfestComposeTheme {
        UserLogin(userName,userPassword)
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DevfestComposeTheme {
        Greeting("Android")
    }
}